GetBRString = function(src_pt, tgt_pt, metric)
    local unit = metric and 'km' or 'nm'
    local dist = mist.utils.get2DDist(src_pt, tgt_pt)
    local bearing_rad = getBearingRad(src_pt, tgt_pt)
    return mist.tostringBR(bearing_rad, dist, nil, metric) .. unit
end

--- Returns a string of coordinates in a format appropriate for the planes of the
--- provided group. i.e. if the group contains F/A-18Cs then we'll return Degrees Minutes Seconds format
--@param grp The group the coordinates are going to be presented to
--@param position The position (table of x,y,z) coordinates to be translated.
--@return String containing the formatted coordinates. Returns an empty string if either grp or position are nil
CoordsForGroup = function(grp, position)
    if grp == nil or position == nil then return "" end
    local u = grp:getUnit(1)
    if not u then return "" end -- Can't get any units from the group to inspect.

    local groupPlaneType = u:getTypeName()
    return CoordForPlaneType(groupPlaneType, position)
end

--- Given a plane type and position, return a string representing the position in a format useful for that planetype.
--@param planeType String indicating the DCS plane type. See Unit.getTypeName() in DCS Scripting docs.
--@param position The position (table of x,y,z) coordinates to be translated
--@return String of coordinates formatted so they can be useful for the given planeType
CoordForPlaneType = function(planeType, pos)
    local lat,long = coord.LOtoLL(pos)
    local dms = function()
        return mist.tostringLL(lat, long, 0, "")
    end
    local ddm = function()
        return mist.tostringLL(lat, long, 3)
    end
    local ddm2 = function()
        return mist.tostringLL(lat, long, 2)
    end
    local mgrs = function()
        return mist.tostringMGRS(coord.LLtoMGRS(lat,long),4)
    end
    local endms6 = function()
        --return mist.tostringLL(lat, lon, 2, "")
        return tostringViggenLL(lat, long, 2)
    end
    --log("Coordinate for [".. planeType .. "]")
    --If it's not defined here we'll use dms.
    local unitCoordTypeTable = {
        ["Ka-50"] = ddm,
        ["M-2000C"] = ddm,
        ["A-10C"] = mgrs,
        ["AJS37"] = endms6,
        ["F-14B"] = ddm,
        ["FA-18C_hornet"] = ddm2
        -- Everything else will default to dms. Add things here if we need exclusions.
    }
    local f = unitCoordTypeTable[planeType]
    if f then return f() else return dms() end
end

GetCoordinateString = function(grp, pos)
    if pos == nil then return "UNKNOWN" end
    return CoordsForGroup(grp, pos) .. " -- " .. GetBRString(GetCoordinate(grp), pos)
end

-- Per group menu, called on groupspawn
buildMenu = function(Group)
    GroupCommand(Group:getID(), "FARP/WAREHOUSE Locations", nil, function()
        local output = [[ALPHA   FARP: 24°41'06"N  55°04'02"E
BRAVO   FARP: 24°43'51"N  55°36'57"E
CHARLIE FARP: 25°46'01"N  55°55'07"E
DELTA   FARP: 26°37'16"N  56°16'42"E]]
        MessageToGroup( Group:getID(), output, 60 )
    end)

    local MissionMenu = GroupCommand(Group:getID(), "Get Mission Status", nil, function()
        MessageToGroup(Group:getID(), TheaterUpdate("Persian Gulf"), 60)
    end)


    local MissionMenu = GroupMenu(Group:getID(), "Get Current Missions")

    GroupCommand(Group:getID(), "SEAD", MissionMenu, function()
        local sams ="ACTIVE SAM REPORT:\n"
        for group_name, group_table in pairs(game_state["StrategicSAM"]) do
            local type_name = group_table["spawn_name"]
            local callsign = group_table['callsign']
            sams = sams .. "OBJ: ".. callsign .." -- TYPE: " .. type_name ..": \t" .. GetCoordinateString(Group, group_table["last_position"]) .. "\n"
        end
        MessageToGroup(Group:getID(), sams, 60)
    end)

    GroupCommand(Group:getID(), "Air Interdiction", MissionMenu, function()
        local bais ="BAI TASK LAST KNOWN COORDINATES LIST:\n"
        for a,group_table in pairs(game_state["BAI"]) do
            local type_name = group_table["spawn_name"]
            bais = bais .. "OBJ: " .. group_table["callsign"] .. " -- " .. type_name .. ": \t" .. GetCoordinateString(Group, group_table["last_position"]) .. "\n"
        end
        MessageToGroup(Group:getID(), bais, 60)
    end)

    GroupCommand(Group:getID(), "Strike", MissionMenu, function()
        local strikes ="STRIKE TARGET'S LAST KNOWN COORDINATES:\n"
        for group_name,group_table in pairs(game_state["C2"]) do
            local callsign = group_table['callsign']
            strikes = strikes .. "OBJ: " .. callsign .. " -- MOBILE CP: \t" .. GetCoordinateString(Group, group_table["last_position"]) .. "\n"
        end

        for group_name,group_table in pairs(game_state["StrikeTargets"]) do
            local callsign = group_table['callsign']
            local spawn_name = group_table['spawn_name']
            strikes = strikes .. "OBJ: " .. callsign .. " -- " .. spawn_name .. ": \t" .. GetCoordinateString(Group, group_table["last_position"]) .. "\n"
        end

        MessageToGroup(Group:getID(), strikes, 60)
    end)

    GroupCommand(Group:getID(), "Interception", MissionMenu, function()
        local intercepts ="INTERCEPTION TARGETS:\n"
        for group_name, group_table in pairs(game_state["AWACS"]) do
            local g = Group.getByName(group_name)
            local GroupPos = GetCoordinate(Group)
            local group_point = GetCoordinate(g)
            local lat,long = coord.LOtoLL(group_point)
            if lat and long then
                intercepts = intercepts .. "AWACS: " .. group_table["callsign"] .. "\t--\t" .. GetBRString(GroupPos, group_point, true) .. "\n"
            end
        end

        for _,group_name in ipairs(game_state["Tanker"]) do
            local g = Group.getByName(group_name)
            local group_point = GetCoordinate(g)
            intercepts = intercepts .. "Tanker" .. group_name["callsign"] .. "\t--\t" .. GetBRString(GetCoordinate(Group), group_point, true) .. "\n"
        end
        MessageToGroup(Group:getID(), intercepts, 60)
    end)

end

-- Global Menu, available to everyone
XportMenu = CoalitionMenu(coalition.side.BLUE, "Deploy Airfield Security Forces")
FARPXportMenu = CoalitionMenu(coalition.side.BLUE, "Deploy FARP/Warehouse Security Forces")

XportMenus = {}
FARPXportMenus = {}
function setSecurityForcesMenu()
    log("Preparing menus for AI airbase security forces.")
    for phase,abs in ipairs(game_state["Phase Airfields"]) do
        for _, ab in pairs(abs) do
            local isFarp = false
            local name = ab:getName()
            local sound = ableavesound
            local menu = XportMenu
            if string.match(name, 'FARP') then
                isFarp = true
                sound = farpleavesound
                menu = FARPXportMenu
            end
            local curMenu = CoalitionCommand(coalition.side.BLUE, "Deploy to " .. name, menu, function()
                log("Requested security forces deploy to " .. name)
                local new_spawn_time = SpawnDefenseForces(ab, timer.getAbsTime() + env.mission.start_time, game_state["last_launched_time"])
                if new_spawn_time ~= nil then
                    trigger.action.outSoundForCoalition(2, sound)
                    game_state["last_launched_time"] = new_spawn_time
                end
            end)
            if isFarp then
                table.insert(FARPXportMenus, curMenu)
            else
                table.insert(XportMenus, curMenu)
            end
        end
    end
    log("Done preparing menus for AI Airbase security forces")
end
setSecurityForcesMenu()

function groupBirthHandler( Event )
    if Event.id ~= world.event.S_EVENT_BIRTH then return end
    if not Event.initiator then return end
    if not Event.initiator.getGroup then return end
    local grp = Event.initiator:getGroup()
    if grp then
        for i,u in ipairs(grp:getUnits()) do
            if u:getPlayerName() and u:getPlayerName() ~= "" then
                buildMenu(grp)
            end
        end
    end
end
mist.addEventHandler(groupBirthHandler)
log("Event Handler complete")
log("menus.lua complete")
