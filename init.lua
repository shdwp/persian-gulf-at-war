local module_folder = lfs.writedir()..[[Scripts\PGAW\]]
package.path = module_folder .. "?.lua;" .. package.path
local ctld_config = require("ctld_config")

local statefile = io.open(lfs.writedir() .. "Scripts\\PGAW\\state.json", 'r')

-- Enable slotblock
trigger.action.setUserFlag("SSB",100)
if statefile then
    trigger.action.outText("Found a statefile.  Processing it instead of starting a new game", 40)
    local state = statefile:read("*all")
    statefile:close()
    local saved_game_state = json:decode(state)
    trigger.action.outText("Game state read", 10)

    -- Spawn any defenders not killed last round
    for template_name, group_name in pairs(saved_game_state["Airfield Defenders"]) do
        local spawner = Spawner(template_name)
        local new_grp = spawner:Spawn()
        game_state["Airfield Defenders"][template_name] = new_grp:getName()
        log("Spawned surviving AP DEF group " .. template_name)
    end

    -- Spawn a defense force if blue owns an airfield.  Set the slotblock flag for the airfield if it needs it
    for name, coalition in pairs(saved_game_state["Airfields"]) do
        if not string.match(name, "Unit") and not string.match(name, "Al Dhafra AB") then
            log("Processing Airfield " .. name)
            local flagval = 100
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p
            local posx = 0
            local posy = 0
            if string.match("Sas Al Nakheel Airport", name) then
                posx = apV3.x
                posy = apV3.z
            elseif string.match("Sir Abu Nuayr", name) then
                posx = apV3.x + 1990.0 
                posy = apV3.z - 1670.0
            elseif string.match("Sirri Island", name) then
                posx = apV3.x + 1872.8 
                posy = apV3.z +  163.8
            elseif string.match("Abu Musa Island Airport", name) then
                posx = apV3.x +  749.4 
                posy = apV3.z -  832.3
            elseif string.match("Tunb Kochak", name) then
                posx = apV3.x +  261.0 
                posy = apV3.z -  289.8
            elseif string.match("Tunb Island AFB", name) then
                posx = apV3.x -  851.9 
                posy = apV3.z +  397.3                            
            else
                posx = apV3.x + math.random(900, 1100)
                posy = apV3.z - math.random(150, 270)
            end
            game_state["Airfields"][name] = coalition

            if coalition == 1 then
                -- just set the AB spawns flag if we need it.  The defense forces left already spawned above.
                if AirbaseSpawns[name] then flagval = 100 end
            elseif coalition == 2 then
                if not string.match(name, "Al Dhafra AB") then
                    log("Spawning Airfield Defense at " .. name)
                    -- Spawn a defense force, a support force, and active a logi slot if one exists for this AB
                    AirfieldDefense:SpawnAtPoint({
                        x = posx,
                        y = posy
                    })

                posx = posx + math.random(100, 200)
                posy = posy + math.random(100, 200)
                BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=posx, y=posy})
                flagval = 0

                --if ab_logi_slots[name] then
                --    activateLogi(ab_logi_slots[name])
                --end
                end
            end

            if abslots[name] then
                for i,grp in ipairs(abslots[name]) do
                    trigger.action.setUserFlag(grp, flagval)
                end
            end
        end
    end

    trigger.action.outText("Finished processing airfields", 10)

    local markerID = 1
    for name, coalition in pairs(saved_game_state["FARPS"]) do
        if not string.match(name, "Abu Dhabi Area FARP") then
            log("Processing FARP " .. name)
            local flagval = 100
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p

            --trigger.action.markToAll(number id, string text, table vec3 , boolean readOnly, string message)
            if string.match("FARP Qeshm", name) then
                trigger.action.markToAll(markerID, name, {x = apV3.x, z = apV3.z+20.0, y = apV3.y}, true)
            else
                trigger.action.markToAll(markerID, name.."\nCapture to provide weapons to Al Dhafra", {x = apV3.x, z = apV3.z+20.0, y = apV3.y}, true)
            end
            markerID = markerID + 1 

            apV3.x = apV3.x + math.random(-25, 25)
            apV3.z = apV3.z + math.random(-25, 25)
            game_state["FARPS"][name] = coalition

            if coalition == 1 then
                flagval = 100
            elseif coalition == 2 then
                BlueSecurityForcesGroups[name] = AirfieldDefense:SpawnAtPoint(apV3)
                apV3.x = apV3.x + 50
                apV3.z = apV3.z - 50
                BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=apV3.x, y=apV3.z}, true)
                flagval = 0

                --if ab_logi_slots[name] then
                --    activateLogi(ab_logi_slots[name])
                --end
            end

            if abslots[name] then
                for i,grp in ipairs(abslots[name]) do
                    trigger.action.setUserFlag(grp, flagval)
                end
            end
        end
    end

    trigger.action.outText("Finished processing FARPs", 10)

    for name, data in pairs(saved_game_state["StrategicSAM"]) do
        local spawn
        if data.spawn_name == "SA6" then spawn = RussianTheaterSA6Spawn[1] end
        if data.spawn_name == "SA10" then spawn = RussianTheaterSA10Spawn[1] end
        spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["C2"]) do
        RussianTheaterC2Spawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["EWR"]) do
        RussianTheaterEWRSpawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    trigger.action.outText("Finished processing strategic assets", 10)

    for name, data in pairs(saved_game_state["StrikeTargets"]) do
        local spawn
        log('spawning ' .. data['spawn_name'])
        if data['spawn_name'] == 'AmmoDump' then spawn = AmmoDumpSpawn end
        if data['spawn_name'] == 'CommsArray' then spawn = CommsArraySpawn end
        if data['spawn_name'] == 'PowerPlant' then spawn = PowerPlantSpawn end
        local static = spawn:Spawn({
            data['position'].x,
            data['position'].z
        })
    end


    for name, data in pairs(saved_game_state["BAI"]) do
        local spawn
        if data['spawn_name'] == "ASSAULT" then spawn = AssaultSpawn[1] end
        if data['spawn_name'] == "ARTILLERY" then spawn = RussianHeavyArtySpawn[1] end

        log("BAI From State is " .. data['spawn_name'])
        local spawnedGroup = spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
        log("Spawned BAI and scheduling tasking. Spawned Group = [ " .. spawnedGroup:getName() .."]")
        mist.scheduleFunction(function()
            local tgt_zone = data['objective']
            log("Sending group [ " .. spawnedGroup:getName() .. " ] to target [ " .. tgt_zone .. " ].")
            mist.groupToPoint(spawnedGroup:getName(), tgt_zone, 'Custom', nil, 50, true)
            game_state["BAI"][spawnedGroup:getName()]['objective'] = tgt_zone
        end, {}, timer.getTime()+ 10)
    end

    trigger.action.outText("Finished processing BAI", 10)

    INIT_CTLD_UNITS = function(args, coords2D, _country, ctld_unitIndex, key)
    --Spawns the CTLD unit at a given point using the ctld_config templates,  
    --returning the unit object so that it can be tracked later.
    --
    --Inputs
    --  args : table
    --    The ctld_config unit template to spawn. 
    --    Ex. ctld_config.unit_config["M818 Transport"]
    --  coord2D : table {x,y}
    --    The location to spawn the unit at.
    --  _country : int or str
    --    The country ID that the spawned unit will belong to. Ex. 2='USA'
    --  cltd_unitIndex : table
    --    The table of unit indices to help keep track of unit IDs. This table
    --    will be accessed by keys so that the indices are passed by reference 
    --    rather than by value.
    --  key : str
    --    The table entry of cltd_unitIndex that will be incremented after a
    --    unit and group name are assigned. 
    --    Ex. key = "Gepard_Index"
    --
    --Outputs
    --  Group_Object : obj
    --    A reference to the spawned group object so that it can be tracked.

        local unitNumber = ctld_unitIndex[key]           
        local CTLD_Group = {
            ["visible"] = false,
            ["hidden"] = false,
            ["units"] = {
              [1] = {
                ["type"] = args.type,                           --unit type
                ["name"] = args.name .. unitNumber,             --unit name
                ["heading"] = 0,
                ["playerCanDrive"] = args.playerCanDrive,
                ["skill"] = args.skill,
                ["x"] = coords2D.x,
                ["y"] = coords2D.y,
              },
            },
            ["name"] = args.name .. unitNumber,                 --group name
            ["task"] = {},
            ["category"] = Group.Category.GROUND,
            ["country"] = _country                              --group country
        }

        --Debug
        --trigger.action.outTextForCoalition(2,"CTLD Unit: "..CTLD_Group.name, 30)

        --Increment Index and spawn unit
        ctld_unitIndex[key] = unitNumber + 1
        local _spawnedGroup = mist.dynAdd(CTLD_Group)

        return Group.getByName(_spawnedGroup.name)              --Group object
    end


    log("START: Spawning CTLD units from state")
    local ctld_unitIndex = ctld_config.unit_index
    for idx, data in ipairs(saved_game_state["CTLD_ASSETS"]) do

        local coords2D = { x = data.pos.x, y = data.pos.z}
        local country = 2   --USA

        if data.name == 'mlrs' then
            local key = "M270_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["MLRS M270"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'M-109' then
            local key = "M109_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M109 Paladin"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'abrams' then
            local key = "M1A1_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M1A1 Abrams"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'jtac' then
            local key = "JTAC_Index"
            local _spawnedGroup = INIT_CTLD_UNITS(ctld_config.unit_config["HMMWV JTAC"], coords2D, country, ctld_unitIndex, key)

            local _code = table.remove(ctld.jtacGeneratedLaserCodes, 1)
            table.insert(ctld.jtacGeneratedLaserCodes, _code)
            ctld.JTACAutoLase(_spawnedGroup:getName(), _code)
        end

        if data.name == 'ammo' then
            local key = "M818_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M818 Transport"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'gepard' then
            local key = "Gepard_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["Flugabwehrkanonenpanzer Gepard"], coords2D, country, ctld_unitIndex, key)
        end

        --if data.name == 'vulcan' then
        --    local key = "Vulcan_Index"
        --    INIT_CTLD_UNITS(ctld_config.unit_config["M163 Vulcan"], coords2D, country, ctld_unitIndex, key)
        --end

        if data.name == 'avenger' then
            local key = "Avenger_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M1097 Avenger"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'chaparral' then
            local key = "Chaparral_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M48 Chaparral"], coords2D, country, ctld_unitIndex, key)
        end
    end
    log("COMPLETE: Spawning CTLD units from state")

    local destroyedStatics = saved_game_state["DestroyedStatics"]
    if destroyedStatics ~= nil then
        for k, v in pairs(destroyedStatics) do
            local obj = StaticObject.getByName(k)
            if obj ~= nil then
                StaticObject.destroy(obj)
            end
        end
        game_state["DestroyedStatics"] = saved_game_state["DestroyedStatics"]
    end

    local CTLDstate = saved_game_state["Hawks"]
    if CTLDstate ~= nil then
        for k,v in pairs(CTLDstate) do
            respawnHAWKFromState(v)
        end
    end

    game_state["CTLD_ASSETS"] = saved_game_state["CTLD_ASSETS"]

else
    -- Populate the world and gameplay environment.
    trigger.action.outText("No state file detected.  Creating new situation", 10)
    for i=1, 6 do
        local zone_index = math.random(20)
        local zone = "NorthSA6Zone"
        RussianTheaterSA6Spawn[1]:SpawnInZone(zone .. zone_index)
    end

    for i=1, 7 do
        if i < 4 then
            local zone_index = math.random(8)
            local zone = "NorthSA10Zone"
            RussianTheaterSA10Spawn[1]:SpawnInZone(zone .. zone_index)
        end

        local zone_index = math.random(8)
        local zone = "NorthSA10Zone"
        RussianTheaterEWRSpawn[1]:SpawnInZone(zone .. zone_index)

        local zone_index = math.random(8)
        local zone = "NorthSA10Zone"
        RussianTheaterC2Spawn[1]:SpawnInZone(zone .. zone_index)
    end

    for i=1, 10 do
        local zone_index = math.random(18)
        local zone = "NorthStatic" .. zone_index
        local StaticSpawns = {AmmoDumpSpawn, PowerPlantSpawn, CommsArraySpawn}
        local spawn_index = math.random(3)
        local vec2 = mist.getRandomPointInZone(zone)
        local id = StaticSpawns[spawn_index]:Spawn({vec2.x, vec2.y})
    end

    for gidx, group in ipairs(coalition.getGroups(1, 2)) do
        local groupname = Group.getName(group)
        if string.match(groupname, "AP DEF") then
            local spawned_group = Spawner(groupname):Spawn()
            game_state["Airfield Defenders"][groupname] = spawned_group:getName()
        end
    end

    -- Disable slots
    for idx=0,2 do
        for abid, ab in pairs(coalition.getAirbases(idx)) do
            for i,ac in ipairs(late_unlockables) do
                if string.match(ac, ab:getName()) then
                    trigger.action.setUserFlag(ac,100)
                    log("Setting flag: " .. ac .. " to 100")
                end
            end
        end
    end
end

-- Kick off supports
mist.scheduleFunction(function()
    -- Friendly
    TexacoSpawn:Spawn()
    ShellSpawn:Spawn()
    OverlordSpawn:Spawn()
    ArcoSpawn:Spawn()
    -- Enemy
    RussianTheaterAWACSSpawn:Spawn()
end, {}, timer.getTime() + 10)

mist.scheduleFunction(function()
  RussianTheaterCASSpawn:Spawn()
  RussianTheaterCASSpawnF14:Spawn()
  RussianTheaterCASSU24Spawn:Spawn()
  log("Spawned CAS Groups...")
end, {}, timer.getTime() + 10, 1800)
-- Kick off the commanders
mist.scheduleFunction(russian_commander, {}, timer.getTime() + 120, 1000)

-- Check base ownership
mist.scheduleFunction(getBaseOwners, {logiSlots}, timer.getTime() + 10, 120)

log("init.lua complete")
