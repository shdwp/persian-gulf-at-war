-- Setup an initial state object and provide functions for manipulating that state.
objective_spawn_zones = {
    ["spawn_1_1"] = Airbase.getByName("Al Maktoum Intl"),
    ["spawn_1_2"] = Airbase.getByName("Al Ain International Airport"),
    ["spawn_1_3"] = Airbase.getByName("FARP Bravo"),
    ["spawn_2_1"] = Airbase.getByName("Dubai Intl"),
    ["spawn_2_2"] = Airbase.getByName("Fujairah Intl"),
    ["spawn_2_3"] = Airbase.getByName("Sharjah Intl"),
    ["spawn_3_1"] = Airbase.getByName("Khasab"),
    ["spawn_3_2"] = Airbase.getByName("Khasab"),
    ["spawn_3_3"] = Airbase.getByName("Khasab"),
}

game_state = {
    ["last_launched_time"] = 0,
    ["last_cap_spawn"] = 0,
    ["Phase Airfields"] = {},
    ["Airfields"] = {},
    ["Airfield Defenders"] = {},
    ["Primary"] = {
        ["Bandar Abbas Intl"] = false,
    },
    ["StrategicSAM"] = {},
    ["C2"] = {},
    ["EWR"] = {},
    ["CASTargets"] = {},
    ["StrikeTargets"] = {},
    ["InterceptTargets"] = {},
    ["DestroyedStatics"] = {},
    ["OpforCAS"] = {},
    ["CAP"] = {},
    ["BAI"] = {},
    ["AWACS"] = {},
    ["Tanker"] = {},
    ["NavalStrike"] = {},
    ["CTLD_ASSETS"] = {},
    ['Convoys'] ={},
    ["last_redfor_cap"] = 0,
    ["FARPS"] = {}
}


local ignored_airfields = {
    --We don't want these to appear in the 'Airfields' list in the state.
    "Stennis",
    "Tarawa",
    "Oliver Hazzard"
}
local function is_ignored_airfield(abName)
    for _,s in ipairs(ignored_airfields) do
        if string.match(abName, s) then return true end
    end
    return false
end
local airfields
for i=0,2 do
    airfields = coalition.getAirbases(i)
    for idx,ab in ipairs(airfields) do
        local name = ab:getName()
        if string.match(name, 'FARP') then
            game_state["FARPS"][name] = i
        elseif not is_ignored_airfield(name) then
            game_state["Airfields"][name] = i
        end
    end
end

local aircraft
abslots = {}
logislots = {}
for ci = 0,2 do
    airfields = coalition.getAirbases(ci)
    aircraft = coalition.getGroups(2)
    for i,ab in ipairs(airfields) do
        logislots[ab:getName()] = nil
        abslots[ab:getName()] = {}
        for i,ac in ipairs(aircraft) do
            if string.match(ac:getName(), ab:getName()) then
                table.insert(abslots[ab:getName()], ac:getName())
            end
        end
    end
end

game_stats = {
    c2    = {
        alive = 0,
        nominal = 6,
        tbl   = game_state["C2"],
    },
    sam = {
        alive = 0,
        nominal = 7,
        tbl   = game_state["StrategicSAM"]
    },
    ewr = {
        alive = 0,
        nominal = 6,
        tbl   = game_state["EWR"],
    },
    awacs = {
        alive = 0,
        nominal = 1,
        tbl   = game_state["AWACS"],
    },
    bai = {
        alive = 0,
        nominal = 5,
        constructing_sam = false,
        tbl = game_state["BAI"],
    },
    ammo = {
        alive = 0,
        nominal = 4,
        tbl   = game_state["StrikeTargets"],
        subtype = "AmmoDump",
    },
    comms = {
        alive = 0,
        nominal = 5,
        tbl   = game_state["StrikeTargets"],
        subtype = "CommsArray",
    },
    caps = {
        alive = 0,
        nominal = 6,
        tbl = game_state["CAP"],
    },
    airports = {
        alive = 0,
        nominal = 3,
        tbl = game_state["Airfields"],
    },
}

game_state["Phase Airfields"] =  {
    {Airbase.getByName("Al Ain International Airport"), Airbase.getByName("Al Maktoum Intl"), Airbase.getByName("FARP ALPHA"), Airbase.getByName("FARP BRAVO")},
    {Airbase.getByName("Al Minhad AB"), Airbase.getByName("Dubai Intl"), Airbase.getByName("Fujairah Intl"), Airbase.getByName("Sharjah Intl")},
    {Airbase.getByName("FARP Charlie"), Airbase.getByName("FARP Delta"), Airbase.getByName("Khasab"), Airbase.getByName("FARP Qeshm")}
}

log("Creating last airbase state")
last_airbase_state = {
    ['FARPS'] = mist.utils.deepCopy(game_state['FARPS']),
    ['Airfields'] = mist.utils.deepCopy(game_state['Airfields'])
}

logiSlots = {}

late_unlockables = {
    "Al Minhad AB Ka-50 - 1",
    "Al Minhad AB Ka-50 - 2",
    "Al Minhad AB Ka-50 - 3",
    "Al Minhad AB Ka-50 - 2",
    "Al Minhad AB Gazelle L - 1",
    "Al Minhad AB Gazelle L - 2",
    "Al Minhad AB Gazelle M - 1",
    "Al Minhad AB Gazelle M - 2",
    "Al Minhad AB UH-1H - 1",
    "Al Minhad AB UH-1H - 2",
    "Al Minhad AB UH-1H - 3",
    "Al Minhad AB UH-1H - 4",
    "Al Minhad AB Mi-8 - 1",
    "Al Minhad AB Mi-8 - 2",
    "Al Minhad AB Mi-8 - 3",
    "Al Minhad AB Mi-8 - 4",
    "FARP Qeshm UH-1H - 1",
    "FARP Qeshm UH-1H - 2",
    "FARP Qeshm UH-1H - 3",
    "FARP Qeshm UH-1H - 4",
    "FARP Qeshm Mi-8 - 1",
    "FARP Qeshm Mi-8 - 2",
    "Al Minhad AB  A-10C - 4",
    "Al Minhad AB  A-10C - 3",
    "Al Minhad AB  A-10C - 2",
    "Al Minhad AB  A-10C - 1",
    "Khasab A-10C - 1",
    "Khasab A-10C - 2",
    "Khasab A-10C - 3",
    "Khasab A-10C - 4",
    "Fujairah Intl Mi-8 - 2",
    "Fujairah Intl Mi-8 - 1",
    "Fujairah Intl UH-1H - 2",
    "Fujairah Intl UH-1H - 1",
    "FARP Charlie Mi-8 - 1",
    "FARP Charlie Mi-8 - 2",
    "FARP Charlie UH-1H - 1",
    "FARP Charlie UH-1H - 2",
    "FARP Delta UH-1H - 1",
    "FARP Delta UH-1H - 2",
    "FARP Delta Mi-8 - 1",
    "FARP Delta Mi-8 - 2",
    "Khasab Mi-8 - 1",
    "Khasab Mi-8 - 2",
    "Khasab UH-1H - 1",
    "Khasab UH-1H - 2",
    "Fujairah Intl Ka-50 - 1",
    "Fujairah Intl Ka-50 - 2",
    "Fujairah Intl Gazelle L - 1",
    "Fujairah Intl Gazelle M - 1",
    "FARP Charlie Gazelle M - 1",
    "FARP Charlie Gazelle M - 2",
    "FARP Charlie Gazelle L - 1",
    "FARP Charlie Gazelle L - 2",
    "FARP Charlie Ka-50 - 1",
    "FARP Charlie Ka-50 - 2",
    "FARP Charlie Ka-50 - 3",
    "FARP Charlie Ka-50 - 4",
    "FARP Delta Gazelle M - 1",
    "FARP Delta Gazelle M - 2",
    "FARP Delta Gazelle L - 1",
    "FARP Delta Gazelle L - 2",
    "FARP Delta Ka-50 - 1",
    "FARP Delta Ka-50 - 2",
    "FARP Delta Ka-50 - 3",
    "FARP Delta Ka-50 - 4",
    "Qeshm Island Gazelle M - 1",
    "Qeshm Island Gazelle M - 2",
    "Qeshm Island Gazelle L - 1",
    "Qeshm Island Gazelle L - 2",
    "Qeshm Island Ka-50 - 1",
    "Qeshm Island Ka-50 - 2",
    "Qeshm Island Ka-50 - 3",
    "Qeshm Island Ka-50 - 4",
}

log("Game State INIT")
